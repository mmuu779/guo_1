import service from "~/utils/axios";

export function doLogin(data){
    return service({
        url: '/auth/doLogin',
        method: 'POST',
        data: data,

    })
}