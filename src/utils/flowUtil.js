export function uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";
 
    var uuid = s.join("");
    return uuid;
}

function removeSubNode(nodeData, param){
    for(const j  in nodeData){
        const childNodes = nodeData[j]
        for(const k in childNodes){
            if(childNodes[k].nodeId == param.nodeId){
                childNodes.splice(k, 1);
                if(k== 0){
                    //删除的为子节点的第一个，表明这个分支都要删除
                    nodeData.splice(j, 1)
                }
            }
        }
    }
}

export function removeNode(nodeData,param){
    for(const i in nodeData){
        if(nodeData[i].nodeId === param.nodeId){
            nodeData.splice(i,1);
            break;
            
        }else{
            let subNode =  nodeData[i].childNodes;
            if(subNode){
               
               removeSubNode(nodeData[i].childNodes, param);

                // 只有2个子节点， 设置允许删除
               if(subNode.length == 2 && subNode.nodeType == 'serial'){ 
                    subNode[1][0].enableDel = true;
               }
               
               if(subNode.length <= 1){
                    // 子节点是否只有2个(注意： 这里的数据结构为 [a,b],[c,d], 移除的子节点可能时 a或c, 但是数据长度不变)
                    nodeData.splice(i,1);
               }else{
                for(let m in subNode){
                    removeNode(subNode[m], param);
                }
               }
                
            }
            
         }
    }
    
}

export function addNode(nodeData,param){
    for(const i in nodeData){
        if(nodeData[i].nodeId === param.nodeId){
            if(param.nodeType === 'serial'){
                nodeData.splice(parseInt(i)+1, 0, {
                        nodeId: uuid(),
                        nodeType: 'serial',
                        nodeName:"分支选择",
                        childNodes:[
                              [
                                {
                                  nodeId: uuid(),
                                  nodeType: 'serial-node',
                                  nodeName:"条件",
                                  sortNum: 0,
                                  value: '',
                                },
                              
                              ],
                              [
                                {
                                  nodeId:  uuid(),
                                  nodeType: 'serial-node',
                                  nodeName:"条件",
                                  value: '其他条件走此流程',
                                  sortNum: 1,
                                  default: true,
                                },
                             
                              ]
                        ]
                    
                })
            }else if(param.nodeType === 'parallel'){
                nodeData.splice(parseInt(i)+1, 0, {
                    nodeId: uuid(),
                    nodeType: 'parallel',
                    nodeName:"并行分支",
                    childNodes:[
                          [
                            {
                                nodeId: uuid(),
                                nodeName:'审批人1',
                                nodeType: 'parallel-node',
                                value: '',
                            },
                          
                          ],
                          [
                            {
                                nodeId: uuid(),
                                nodeName:'审批人2',
                                nodeType: 'parallel-node',
                                value: '',
                            },
                         
                          ]
                    ]
                
                })  
            }else{
                nodeData.splice(parseInt(i)+1, 0, {
                    nodeId: uuid(),
                    nodeName:'审批人',
                    nodeType:param.nodeType,
                    value: '',
                })
            }
            break;
            
        }else{
            let subNode =  nodeData[i].childNodes;
            for(let m in subNode){
                addNode(subNode[m], param);
            }

         }
    }
    
}

export function changeNodeStatus(nodeData, hisList){
    // 修改节点状态

    for(const i in nodeData){
        nodeData[i].nodeStatus = [];
        const hiss = hisList.filter(r => r.nodeCode == nodeData[i].nodeId);
        if(hiss && hiss.length > 0){
            for(const j in hiss){
                nodeData[i].nodeStatus.push(hiss[j].flowStatus);
            }
            
        }else{
            let subNode =  nodeData[i].childNodes;
            if(subNode){
               for(let m in subNode){
                   changeNodeStatus(subNode[m], hisList);
               }
            }
        }
            
    }

}


export function nodeList(nodeData, list){
    for(const i in nodeData){
       const node =  nodeData[i];
       if(node.nodeType == 'start' || node.nodeType == 'between'   // || node.nodeType == 'parallel'  || node.nodeType == 'serial' 
           || node.nodeType == 'parallel-node'){
           list.push({
               nodeId: node.nodeId,
               nodeName: node.nodeName,
               nodeType: node.nodeType,
           });
       }
       
       const childNodes = node.childNodes;
       if(childNodes != null && childNodes.length > 0){
            for(const j in childNodes){
                nodeList(childNodes[j], list);
            }    
       }
        

    }
}