import router from "./index.js"
import { useUserStore } from "~/store/user";
import { usePermissionStore } from "~/store/permission.js";
import Layout from '~/components/layouts/index.vue';
import { reactive } from "vue";
import { storeToRefs } from "pinia";


router.beforeEach((to, from, next) => {
    const permissionStore = usePermissionStore();
    const userStore = useUserStore();
    if (userStore.token) {
        if(to.name == 'Login'){
            next()
        }else if( permissionStore.menuRouter.length == 0) {
            // 获取菜单
           permissionStore.loadMenuRouter().then((result) => {
           
            result.forEach((item) => {
                router.addRoute(item)
            })
           
            router.addRoute( reactive({ path: '/index', 
                name:"BASE", 
                // redirect: '/home',
                component: Layout,
                children: permissionStore.baseRouter
                
              }))
            

            
            next({ ...to, replace: true })
           });
       }


      next()
    }else if(to.name == 'Login'){
        next()
    }


    else {
      next({name: 'Login'})
    }
  })
