import { createMemoryHistory, createRouter } from 'vue-router'

import Login from '~/views/login.vue'
import Layout from '~/components/layouts/index.vue';
import Home from '~/views/home/index.vue'
import FlowConfig from '~/views/flowconfig/index.vue'
const routes = [
  { path: '/', 
    name:"Layout" , 
    redirect: '/home',
    component: Layout,
    children:[
      // { path: '/index', component: Home, meta:{title: "首页"} },
      { path: '/home', component: Home, meta:{title: "首页"} },
     
      
      { path: '/flow/flowconfig/:id', component: FlowConfig, name: "myconfig" , meta:{title: "流程配置"} },

      
     
    ] 
  },

  { path: '/login', component: Login, name: "Login"  },
]

const router = createRouter({
  history: createMemoryHistory(),
  routes,
})



export default router;