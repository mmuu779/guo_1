// 用户权限 store，大致看了一下若依框架的源码，自己实现一套逻辑

import { defineStore } from 'pinia'
import { menuRouter } from '~/api/sys/menu'
import Layout from '~/components/layouts/index.vue';
import router from '../router';

// import { baseRouter } from '../router/baseRouter';
export const usePermissionStore = defineStore('permission', {
  state: () =>({
    // 菜单列表
    routers: [],
    menuList:[],
    baseRouter:[]

  }),
  getters:{
    menuRouter : (state) => state.routers,
    menuTree : (state) => state.menuList,
    layoutRouter : (state) => state.baseRouter
  },
  // 也可以这样定义
  // state: () => ({ count: 0 })
  actions: {
    
    // 获取当前用户的菜单
    loadMenuRouter() {
      var that = this;
    
      // const uuid = userInfo.uuid
       return new Promise((resolve, reject) => {
        menuRouter().then(res => {
          // setToken(res.data.access_token)
          that.menuList =   JSON.parse(JSON.stringify(res.rows));
          const data = JSON.parse(JSON.stringify(res.rows));
          that.baseRouter.push(...firstMenu(data))
          const routerList  =  convertToRouter(data);
           console.log("routerList")
          console.log(routerList)
          // that.routers.push(...baseRouter)
          that.routers.push(...routerList);
          resolve(that.routers)
        }).catch(error => {
          reject(error)
        })
      })
    },
  
    
  },
  persist: {
    enabled: false
  }
})


const firstMenu = (data) => {
  let baseRouter = [];
  data.forEach(item => {
    if(item.type == '2'){
      const view =`/src/views/${item.component}.vue`
      item.component = () => import(view) 
      item.meta = {title: item.name}
      baseRouter.push(item);
    }
  })
  return baseRouter
}

const convertToRouter = (menuList, istop = true) => {
  const menuListResult = [];

  menuList.forEach(item => {
    if(item.type == '1'){
      item.component = Layout
      menuListResult.push(item);
    }else if(item.type == '2' && !istop){
        const view =`/src/views/${item.component}.vue`
        item.component = () => import(view) 
        menuListResult.push(item);
     
    }
    item.meta = {title: item.name}
    if(item.children){
     const children  = convertToRouter(item.children, false);
     item.children = children;
    }
     
  })
  return menuListResult;
}
