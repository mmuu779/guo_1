//  用户信息
// stores/counter.js
import { defineStore } from 'pinia'
import { doLogin } from '~/api/auth/login'
import router from '~/router/index'
  //login登录页放入token， 然后接口请求时取token
export const useUserStore = defineStore('user', {
  state: () =>({
    access_token: '',
  }),
  getters:{
    token: (state) => state.access_token
  },
  // 也可以这样定义
  // state: () => ({ count: 0 })
  actions: {
    loginByAccount(userInfo) {
      var that = this;
      userInfo.username = userInfo.username.trim()
      // const uuid = userInfo.uuid
       return new Promise((resolve, reject) => {
        doLogin(userInfo).then(res => {
          // setToken(res.data.access_token)
          that.access_token = res.data
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    logout() {
      this.access_token = '';
      router.push({name: 'Login'})
    },
    login(data) {
      doLogin(data).then(res=> {
          // 获取token, 保存
      });
        //  登录逻辑处理
    },
  },
  persist: {
    enabled: true
  }
})